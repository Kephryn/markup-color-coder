<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:php="http://php.net/xsl"
	xsl:extension-element-prefixes="php"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- &#09; tab -->

	<xsl:namespace-alias stylesheet-prefix="php" result-prefix="xsl" />

	<xsl:template match="/">
		<pre class="mcc" style="background: {$background}; padding: 10px 0;margin-left: 0;">
			<xsl:apply-templates/>
		</pre>
	</xsl:template>

	<xsl:template match="comment()">
		<div style="color: {$comment}; margin-left: 10px; border-left: 1px dashed #333; padding-left:2px;">&lt;!--<xsl:value-of select="."/>--&gt;</div>
	</xsl:template>

	<xsl:template match="//*">
		<div style="margin-left: 10px; border-left: 1px dashed {$ruler}; padding-left:2px;">
			<span style="color: {$bracket}">&lt;</span>
			<xsl:choose>
				<xsl:when test="@*">
					<span class="tag" id="{generate-id()}-tag">
						<span style="color: {$node}"><xsl:value-of select="name()"/></span>
						<span id="{generate-id(.)}">
							<xsl:for-each select="@*">
								<span style="color: {$bracket}">&#160;<span style="color: {$attribute}"><xsl:value-of select="name()"/></span>=</span>
								<span style="color: {$attribute-value}">
									<xsl:choose>
										<xsl:when test="substring(.,1,2) = '//'">
											<xsl:text>&quot;</xsl:text>
											<a href="{.}" target="_blank"><xsl:value-of select="."/></a>
											<xsl:text>&quot;</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>&quot;</xsl:text>
											<xsl:value-of select="."/>
											<xsl:text>&quot;</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</span>
							</xsl:for-each>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color: {$node}"><xsl:value-of select="name()"/></span>
				</xsl:otherwise>
			</xsl:choose>
				<!-- namespace::*[name()=''] -->

			<xsl:if test="namespace-uri(.) != namespace-uri(parent::*)">
			   	<span style="color: {$bracket}">&#160;<span style="color: {$namespace}">xmlns</span>=</span>
				<span style="color: {$namespace-value}">
					<xsl:text>&quot;</xsl:text>
					<xsl:value-of select="namespace-uri(.)" />
					<xsl:text>&quot;</xsl:text>
				</span>
			</xsl:if>

			<xsl:choose>
				<xsl:when test=".//* or text()">
					<span style="color: {$bracket}">&gt;</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color: {$bracket}">/&gt;</span>
				</xsl:otherwise>
			</xsl:choose>

			<span id="{generate-id(.)}-content" style="color: {$node-value}"><xsl:apply-templates/></span>

			<xsl:if test=".//* or text()">
				<span style='white-space:nowrap'>
					<span style="color: {$bracket}">&lt;</span>
					<span style="color: {$node}">/<xsl:value-of select="name()"/></span>
					<span style="color: {$bracket}">&gt;</span>
				</span>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>