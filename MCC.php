<?php
namespace Kephryn;

use DOMDocument;
use XSLTProcessor;
/**
 * Markup Colour Coder
 *
 * She still lives after 10+ years!
 *
 * @author Yvon Viger <yvon@baytek.ca>
 */
class MCC
{
	private $document;
	private $processor;
	private $stylesheet;
	private $xsl = 'mcc.xsl';

	private $colors = [
		'background' => 'transparent',
		'foreground' => 'black',
		'bracket' => '#bbb',
		'comment' => '#999',
		'ruler' => '#ddd',
		'node' => '#1299DA',
		'node-value' => 'black',
		'attribute' => '#56DB3A',
		'attribute-value' => '#FF8400',
		'namespace' => 'red',
		'namespace-value' => 'purple',
	];

	public function __construct($xml = null) { //Don't type hint because we just want xml of any kind

		// Load the XSLTProcessor
		$this->processor = new XSLTProcessor;
		if (!$this->processor->hasExsltSupport()) {
		    throw new Exception('EXSLT support not available, server configuration issue!');
		}

		// Create the stylesheet, load and import
		$this->stylesheet = new DOMDocument;
		$this->stylesheet->load(__DIR__ . DIRECTORY_SEPARATOR . $this->xsl);
		$this->processor->registerPHPFunctions(); // Not really required but there just in case
		$this->processor->importStylesheet($this->stylesheet);

		if(is_string($xml)) {
			// Our input XML Document
			$this->document = new DOMDocument;
			return (string)$this->load($xml);
		}
		else if(is_object($xml)) {
			$this->document = $xml;
			return (string)$this->document;
		}
	}

	public function load($xml) {
		return $this->document->loadXML($xml);
	}

	public function process() {
		foreach($this->colors as $key => $value) {
			$this->processor->setParameter('', $key, $value);
		}

		return $this->processor->transformToDoc($this->document)->saveXML();
	}

	public function __toString() {
		return $this->process();
	}
}